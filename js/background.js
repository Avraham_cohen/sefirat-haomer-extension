/* global chrome */
var moment = window.moment;
var Hebcal = window.Hebcal;

var today, todayHebrewObj, isAfterSunset, omer;

function setupHebrewDate () {
  todayHebrewObj = Hebcal.HDate(new Date(today.toISOString()));
  todayHebrewObj.setLocation(31.783, 35.233); // Jerusalem
}

function setup () {
  today = moment();
  setupHebrewDate();

  isAfterSunset = today.isAfter(todayHebrewObj.sunset());
  if (isAfterSunset) {
    today = today.add(1, 'days');
    setupHebrewDate();
  }

  omer = todayHebrewObj.omer();

  chrome.browserAction.setBadgeText({ text: omer ? omer.toString() : '' });
  chrome.browserAction.setBadgeBackgroundColor({ color: '#666' });
  reminderToday();
  setTimeout(setup, 43200000); // 12 hours.
}
setup();

function setCron () {
  setTimeout(function () {
    setup();
    setCron();
  }, todayHebrewObj.sunset() - new Date());
}
setCron();

function reminderToday () {
  chrome.storage.sync.get(['options', 'lastUserSfira'], function (data) {
    if (!data.options.reminder || data.lastUserSfira >= omer) {
      return;
    }
    var confirmMsg = `
    היום ${omer} בעומר
    אם כבר ספרת, לחץ "אישור"
    אם אתה מעוניין בתזכורת בעוד חצי שעה, לחץ "ביטול"`
    var isAlreadySafar = confirm(confirmMsg);
    if (isAlreadySafar) {
      chrome.storage.sync.set({ lastUserSfira: omer });
      return;
    }
    setTimeout(reminderToday, 1000 * 60 * 30);
  })
}
